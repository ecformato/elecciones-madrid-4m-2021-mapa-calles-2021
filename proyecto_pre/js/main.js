import { text } from 'd3-request';
import { dsvFormat } from 'd3-dsv';
import '../css/main.scss';
import { isMobile } from '../../common_projects_utilities/js/dom-helpers';

//GRADIENTES DE COLORES
let bloqueIzquierdas = ['#f2edd1', '#f4eab8', '#f5e79f', '#f6e385', '#fbdd49', '#e2c32a'],
    bloqueDerechas = ['#d3d3d1', '#b5b7b8', '#979b9f', '#787e86', '#5a626d', '#3c4554'],
    empates = '#f1f0ea',
    sin_informacion = '#e1e1e1',
    podemos_four_colors = ['#efeaf1','#c4adc2','#966a9a','#692772'],
    masmadrid_four_colors = ['#ebf4f3','#b7ded7','#85cdc5','#20c0b2'],
    psoe_four_colors = ['#f5ebea','#f3a49c','#f4574f','#f60b01'],
    cs_four_colors = ['#f6e9e1','#f2be9f','#f38c54','#f45a09'],
    pp_four_colors = ['#eff3f6','#b9d9f0','#80c1f7','#48aafd'],
    vox_four_colors = ['#ecf2e4','#c8dfab','#a0cf6c','#77be2d'];

mapboxgl.accessToken = 'pk.eyJ1IjoiZGF0b3MtZWxjb25maWRlbmNpYWwiLCJhIjoiY2syMGI5NHV4MDB1OTNnbnJtZ3UweGh5YiJ9.UrXbe3ehqGCgKdgMgHZnBQ'; //Cambiar

/* Ajustar ZOOM, MINZOOM Y CENTER EN FUNCIÓN DEL DISPOSITIVO */
let mapWidth = document.getElementById('map').clientWidth;
let zoom = mapWidth > 525 ? 8 : 7.5;
let minZoom = mapWidth > 525 ? 7 : 7.5;
let center = [-3.75, 40.55];

let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/datos-elconfidencial/ckmowiz7t7pg517qvw94iyz3e',
    attributionControl: false,
    zoom: zoom,
    minZoom: minZoom,
    maxZoom: 16,
    center: center
});

map.addControl(
    new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: mapboxgl,
        language: 'es',
        countries: 'es',
        marker: false,
        placeholder: 'Busque por calle o municipio',
        bbox: [-4.877929,39.880235,-2.988281,41.236511],
        filter: function (item) {
            return item.context
                .map(function (i) {
                    return (
                        i.id.split('.').shift() === 'region' &&
                        i.text === 'Madrid'
                        );
                    })
                .reduce(function (acc, cur) {
                    return acc || cur;
                });
        }
    })
);

/* Variable para poder navegar sobre el mapa */
let nav = new mapboxgl.NavigationControl({showCompass:false});
map.addControl(nav, 'top-right');
map.addControl(new mapboxgl.FullscreenControl());

map.scrollZoom.disable();

map.on('load', function(){
    let file = 'https://raw.githubusercontent.com/CarlosMunozDiazEC/prueba-datos/main/secciones_madrid_mapbox.csv'; //Cambiar por los de 2021

    text(file, function(data){
        let dsv = dsvFormat(";");
        let parseData = dsv.parse(data);

        map.addSource('secciones_censales', {
            'type': 'vector',
            'url': 'mapbox://datos-elconfidencial.aupbtynu',
            promoteId: 'CUSEC'
        });
        
        parseData.map((item) => {
            map.setFeatureState({
                source: 'secciones_censales',
                sourceLayer: 'secciones_cam-a5lgu8',
                id: +item['Cusec']
            }, {
                //Fuerzas políticas
                pp_porc: +item['% PP'].replace(',','.'),
                psoe_porc: +item['% PSOE'].replace(',','.'),
                masmadrid_porc: +item['% MÁS MADRID'].replace(',','.'),
                podemos_porc: +item['% Podemos'].replace(',','.'),
                vox_porc: +item['% VOX'].replace(',','.'),
                cs_porc: +item['% Cs'].replace(',','.'),

                //Otras variables
                primeraFuerza: item['Primera fuerza'].trim(),
                primeraFuerzaPorc: +item['% PF'].replace(',','.'),
                segundaFuerza: item['Segunda fuerza'].trim(),
                segundaFuerzaPorc: +item['% SF'].replace(',','.'),

                participacion_porc: +item['Participación'].replace(',','.'),                
                bloqueGanador: item['Bloque Gana'].trim(),
                izq_porc: +item['Izquierda'].replace(',','.'),
                der_porc: +item['Derecha'].replace(',','.')
            });
        });

        //Coropletas > Primero rellenamos con ganador
        map.addLayer({
            'id': 'secciones_censales_madrid',
            'source': 'secciones_censales',
            'source-layer': 'secciones_cam-a5lgu8',
            'type': 'fill',
            'paint': {
                'fill-color': [
                    'match',
                    ['feature-state', 'primeraFuerza'],
                    "MÁS MADRID", [
                        'step',
                        ['feature-state', 'masmadrid_porc'],
                        masmadrid_four_colors[0],
                        25,
                        masmadrid_four_colors[1],
                        30,
                        masmadrid_four_colors[2],
                        35,
                        masmadrid_four_colors[3]
                    ],
                    "Cs", [
                        'step',
                        ['feature-state', 'cs_porc'],
                        cs_four_colors[0],
                        25,
                        cs_four_colors[1],
                        30,
                        cs_four_colors[2],
                        35,
                        cs_four_colors[3]
                    ],
                    "PSOE", [
                        'step',
                        ['feature-state', 'psoe_porc'],
                        psoe_four_colors[0],
                        25,
                        psoe_four_colors[1],
                        30,
                        psoe_four_colors[2],
                        35,
                        psoe_four_colors[3]
                    ],
                    "PP", [
                        'step',
                        ['feature-state', 'pp_porc'],
                        pp_four_colors[0],
                        25,
                        pp_four_colors[1],
                        30,
                        pp_four_colors[2],
                        35,
                        pp_four_colors[3]
                    ],
                    "PODEMOS - IU", [
                        'step',
                        ['feature-state', 'podemos_porc'],
                        podemos_four_colors[0],
                        25,
                        podemos_four_colors[1],
                        30,
                        podemos_four_colors[2],
                        35,
                        podemos_four_colors[3]
                    ],
                    "VOX", [
                        'step',
                        ['feature-state', 'vox_porc'],
                        vox_four_colors[0],
                        25,
                        vox_four_colors[1],
                        30,
                        vox_four_colors[2],
                        35,
                        vox_four_colors[3]
                    ],
                    "Sin datos", sin_informacion,
                    empates
                ],
                'fill-opacity': 0.75
            }      
        }, 'road-simple');

        //Límites censales
        map.addLayer({
            'id': 'secciones_censales_line',
            'source': 'secciones_censales',
            'source-layer': 'secciones_cam-a5lgu8',
            'type': 'line',
            'paint': {
                'line-color': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    '#000',
                    '#fff'
                ],
                'line-width': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    1.75,
                    0.1
                ]
            }      
        }, 'road-label-simple');
    });
});